import ui from '../../ui'
import './SelectableContent.styl'

export default {
  functional: true,

  props: {
    disabled: {
      type: Boolean,
      default: false
    },
    focusKey: {
      type: String,
      default: 'hasFocus'
    },
    hoverKey: {
      type: String,
      default: 'hasHover'
    },
    tabindex: {
      type: Number,
      default: 0
    }
  },

  render (createElement, context) {
    const {children, parent, props} = context

    const click = e => {
      if (typeof parent.handleClick === 'function') parent.handleClick(e)
      else parent.$emit('click', e)
    }

    const focus = e => {
      if (ui.currentInput === 'touch') return
      parent[props.focusKey] = true
      if (typeof parent.handleFocus === 'function') {
        parent.handleFocus(e)
      }
      parent.$emit('focus')
    }

    const blur = e => {
      parent[props.focusKey] = false
      if (typeof parent.handleBlur === 'function') {
        parent.handleBlur(e)
      }
      parent.$emit('blur')
    }

    const mouseenter = e => {
      parent[props.hoverKey] = true
      if (typeof parent.handleMouseEnter === 'function') {
        parent.handleMouseEnter(e)
      }
      parent.$emit('mouseEnter', e)
    }

    const mouseleave = e => {
      parent[props.hoverKey] = false
      if (typeof parent.handleMouseLeave === 'function') {
        parent.handleMouseLeave(e)
      }
      parent.$emit('mouseLeave', e)
    }

    const keyup = e => {
      const kb = ui.keyCodes
      switch (e.keyCode) {
        case kb.enter: parent.$el.click(); break
        case kb.space: parent.$el.click(); break
        case kb.up: parent.$emit('up'); break
        case kb.down: parent.$emit('down'); break
        case kb.left: parent.$emit('left'); break
        case kb.right: parent.$emit('right'); break
      }
    }

    return createElement(
      'div',
      {
        attrs: {
          disabled: props.disabled,
          tabindex: props.tabindex
        },
        class: ['FocusableContent'],
        on: {
          focus,
          blur,
          click,
          mouseenter,
          mouseleave,
          keyup
        }
      },
      [
        children
      ]
    )
  }
}
