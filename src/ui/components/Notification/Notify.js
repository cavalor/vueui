import cuid from 'cuid'
import {isInteger} from 'lodash'
import {DEBUG} from 'constants'

const _notifications = new WeakMap()
const _notify = new WeakMap()

export default class Notify {
  static SUCCESS = 'success'
  static WARN = 'warn'
  static ERROR = 'error'
  static INFO = 'info'

  constructor () {
    _notifications.set(this, [])
    _notify.set(this, (type, message) => {
      const notification = {
        type,
        message,
        id: cuid(),
        timeLeft: 1000
      }
      const inter = setInterval(() => {
        if (notification.timeLeft > 0) {
          notification.timeLeft--
          if (DEBUG) console.log('notification interval', notification.timeLeft)
        } else {
          notification.dismiss()
        }
      }, 10)
      notification.dismiss = () => {
        const index = this.notifications.findIndex(n => n.id === notification.id)
        clearInterval(inter)
        if (isInteger(index)) {
          this.notifications.splice(index, 1)
        }
      }
      this.notifications.push(notification)
    })
  }

  get notifications () {
    return _notifications.get(this)
  }

  dismiss (id) {
    const index = this.notifications.findIndex(n => n.id === id)
    if (isInteger(index)) {
      this.notifications[index].dismiss()
      // this.notifications.splice(index, 1)
    }
  }

  success (message) {
    _notify.get(this)(Notify.SUCCESS, message)
  }

  warn (message) {
    _notify.get(this)(Notify.WARN, message)
  }

  error (message) {
    _notify.get(this)(Notify.ERROR, message)
  }

  info (message) {
    _notify.get(this)(Notify.INFO, message)
  }
}
