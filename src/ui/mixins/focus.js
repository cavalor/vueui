export default {
  data () {
    return {
      hasFocus: false,
      hadHover: false,
      isDisabled: false
    }
  },

  methods: {
    handleFocus () {
      if (!this.isDisabled) {
        this.hasFocus = true
      }
    },
    handleBlur () {
      this.hasFocus = false
    }
  }
}
