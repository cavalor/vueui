import trackInput from './utils/trackInput'
import {isPc} from './utils'

const keyCodes = {
  space: 32,
  tab: 9,
  enter: 13,
  esc: 27,
  escape: 27,
  left: 37,
  up: 38,
  rigth: 39,
  down: 40
}

const depth = {
  popup: 100,
  current: 200,
  getNext () {
    this.current += 1
    return this.current
  }
}

// UI API
const ui = {
  keyCodes,
  depth,
  isPc: isPc()
}

// Track user input
trackInput(ui)

export default ui
