import './Overlay.styl'

export default {
  name: 'Overlay',

  functional: true,

  props: {
    background: {
      type: [String, null],
      default: 'rgba(64, 64, 64, .95)'
    },
    depth: {
      type: Number,
      default: 1000
    },
    handleClick: {
      type: Function,
      default: null
    }
  },

  render (createElement, { parent, props }) {
    const click = (e) => {
      parent.$emit('overlayClick')
    }

    return createElement('div', {
      class: ['Overlay'],
      style: {
        zIndex: props.depth,
        backgroundColor: props.background
      },
      on: { click }
    })
  }
}
