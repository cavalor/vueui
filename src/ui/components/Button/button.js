export default {
  data () {
    return {
      hasFocus: false,
      hasHover: false
    }
  },

  props: {
    size: Number, // button size in px
    fontSize: Number, // button font size in px
    icon: String, // icon name
    iconSize: Number, // icon size
    small: Boolean, // small button variant
    large: Boolean, // large button variant
    disabled: Boolean, // disabled button state indicator
    loading: Boolean, // loading state indicator
    color: String, // color map
    square: Boolean // remove border radius
  },

  computed: {
    btnSize () {
      if (this.size) {
        return this.size
      }
      return this.large ? 48 : this.small ? 24 : 32
    },
    btnFontSize () {
      if (this.fontSize) {
        return this.fontSize
      }
      return this.large ? 20 : this.small ? 14 : 16
    }
  }
}
