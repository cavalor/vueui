export function getBackgroundColor (target) {
  let el = target
  let {backgroundColor} = getComputedStyle(el)
  while (backgroundColor === 'rgba(0, 0, 0, 0)' && el.tagName !== 'body') {
    el = el.parentNode
    backgroundColor = getComputedStyle(el).backgroundColor
  }
  return backgroundColor
}
