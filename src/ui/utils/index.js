export function isPc () {
  const agents = ['Android', 'iPhone', 'Windows Phone', 'iPad', 'iPod']
  const uaInfo = typeof navigator !== 'undefined' ? navigator.userAgent : ''
  for (const agent in agents) {
    if (uaInfo.indexOf(agent) > 0) return true
  }
  return false
}

export function xor (a, b) {
  return (a || b) && !(a && b)
}

export const pc = val => `${val}%`

export const px = val => `${val}px`

