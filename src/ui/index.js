import {upperFirst, upperCase, lowerCase} from 'lodash'
import ui from './ui'

import 'normalize.css'
import './styles/ui.styl'

/**
 * Vue plugin
 */
export default {
  install (Vue) {
    Vue.component('FlatButton', require('./components/Button/FlatButton.vue'))
    Vue.component('IconButton', require('./components/Button/IconButton.vue'))
    Vue.component('Icon', require('./components/Icon.vue'))
    Vue.component('TextField', require('./components/TextField.vue'))
    Vue.component('SelectField', require('./components/SelectField.vue'))
    Vue.component('Checkbox', require('./components/Checkbox.vue'))
    // Vue.component('ButtonGroup', require('./components/ButtonGroup.vue'))
    Vue.component('Popup', require('./components/Popup.vue'))
    Vue.component('Loader', require('./components/Loader.vue'))
    Vue.component('DotLoader', require('./components/DotLoader.vue'))
    Vue.component('Pagination', require('./components/Pagination/Pagination.vue'))
    Vue.component('Tabs', require('./components/Tabs/Tabs.vue'))
    Vue.component('Tab', require('./components/Tabs/Tab.vue'))

    Vue.directive('ripple', require('./directives/ripple').default)
    Vue.directive('focus', require('./directives/focus').default)
    Vue.directive('height', require('./directives/height').default)
    Vue.directive('width', require('./directives/width').default)

    Vue.filter('ucFirst', upperFirst)
    Vue.filter('upperCase', upperCase)
    Vue.filter('lowerCase', lowerCase)
    Vue.filter('floor', Math.floor)
    Vue.filter('round', Math.round)
    Vue.filter('size', require('./filters/size').default)

    Vue.mixin({
      created () {
        this.$ui = ui
      }
    })
  }
}
