import {px} from '../utils'

function adjustHeight (el, bindings) {
  const value = bindings.value || 1
  el.style.height = px(el.clientWidth * value)
}

export default {
  inserted: adjustHeight,
  update: adjustHeight
}
