import './FocusIndicator.styl'

export default {
  name: 'FocusIndicator',

  functional: true,

  props: {
    show: {
      type: Boolean,
      default: false
    },
    radius: {
      type: [String, Number],
      default: '50%'
    },
    offset: {
      type: Number,
      default: 0
    },
    color: {
      type: String,
      default: 'rgba(192,192,192,.5)'
    },
    relativeTo: {
      type: Object,
      default: null
    }
  },

  render (createElement, { props, parent }) {
    if (!props.show) {
      return
    }

    let anchorElement
    if (!props.relativeTo) {
      anchorElement = parent.$el
    } else if (props.relativeTo && props.relativeTo.$el) {
      anchorElement = props.relativeTo.$el
    } else if (props.relativeTo) {
      anchorElement = props.relativeTo
    } else {
      console.warn('Focus indicator anchor not found')
      return
    }

    const isRound = props.radius === '50%'
    const width = anchorElement.clientWidth - props.offset
    const height = isRound ? width : anchorElement.clientHeight - props.offset

    const radius = () => {
      if (typeof props.radius === 'number') {
        return `${props.radius}px`
      }
      return props.radius
    }

    const top = () => (height - anchorElement.clientHeight) / -2

    return createElement('div', {
      class: ['FocusIndicator'],
      style: {
        width: `${width}px`,
        height: `${height}px`,
        top: `${top()}px`,
        left: `${props.offset / 2}px`,
        background: props.color,
        borderRadius: radius(),
        opacity: props.show ? 1 : 0
      }
    })
  }
}
