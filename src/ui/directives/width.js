import {px} from '../utils'

function adjustWidth (el, bindings) {
  const value = bindings.value || 1
  el.style.width = px(el.clientHeight * value)
}

export default {
  inserted: adjustWidth,
  update: adjustWidth
}
