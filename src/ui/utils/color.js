const primaryColor = 'purple'
const secondaryColor = 'lime'

export const maps = {
  'red': ['#faeaea', '#f4cbca', '#eca9a7', '#e48784', '#df6d69', '#d9534f', '#d54c48', '#cf423f', '#ca3936', '#c02926'],
  'green': ['#ebf6eb', '#ceeace', '#aedcae', '#8dcd8d', '#74c374', '#5cb85c', '#54b154', '#4aa84a', '#41a041', '#309130'],
  'blue': ['#E3F2FD', '#BBDEFB', '#90CAF9', '#64B5F6', '#42A5F5', '#2196F3', '#1E88E5', '#1976D2', '#1565C0', '#0D47A1'],
  'yellow': ['#fdf5ea', '#fbe6ca', '#f8d6a7', '#f5c683', '#f2b969', '#f0ad4e', '#eea647', '#ec9c3d', '#e99335', '#e58325'],
  'grey': ['#FAFAFA', '#F5F5F5', '#EEEEEE', '#E0E0E0', '#BDBDBD', '#9E9E9E', '#757575', '#616161', '#424242', '#212121'],
  'navy': ['#e9eaf0', '#bbbdd1', '#9a9cbb', '#6f739e', '#60648e', '#54577c', '#484a6a', '#3b3d58', '#2f3145', '#232433'],
  'purple': ['#e6d1e8', '#cb9ecf', '#b778bd', '#984e9f', '#85448a', '#713a76', '#5d3061', '#4a264d', '#361c38', '#221224'],
  'pink': ['#FCE4EC', '#F8BBD0', '#F48FB1', '#F06292', '#EC407A', '#E91E63', '#D81B60', '#C2185B', '#AD1457', '#880E4F'],
  'indigo': ['#E8EAF6', '#C5CAE9', '#9FA8DA', '#7986CB', '#5C6BC0', '#3F51B5', '#3949AB', '#303F9F', '#283593', '#1A237E'],
  'cyan': ['#E0F7FA', '#B2EBF2', '#80DEEA', '#4DD0E1', '#26C6DA', '#00BCD4', '#00ACC1', '#0097A7', '#00838F', '#006064'],
  'teal': ['#E0F2F1', '#B2DFDB', '#80CBC4', '#4DB6AC', '#26A69A', '#009688', '#00897B', '#00796B', '#00695C', '#004D40'],
  'lime': ['#F9FBE7', '#F0F4C3', '#E6EE9C', '#DCE775', '#D4E157', '#CDDC39', '#C0CA33', '#AFB42B', '#9E9D24', '#827717'],
  'amber': ['#FFF8E1', '#FFECB3', '#FFE082', '#FFD54F', '#FFCA28', '#FFC107', '#FFB300', '#FFA000', '#FF8F00', '#FF6F00'],
  'orange': ['#FFF3E0', '#FFE0B2', '#FFCC80', '#FFB74D', '#FFA726', '#FF9800', '#FB8C00', '#F57C00', '#EF6C00', '#E65100'],
  'deepOrange': ['#FBE9E7', '#FFCCBC', '#FFAB91', '#FF8A65', '#FF7043', '#FF5722', '#F4511E', '#E64A19', '#D84315', '#BF360C'],
  'brown': ['#EFEBE9', '#D7CCC8', '#BCAAA4', '#A1887F', '#8D6E63', '#795548', '#6D4C41', '#5D4037', '#4E342E', '#3E2723'],
  'blueGrey': ['#ECEFF1', '#CFD8DC', '#B0BEC5', '#90A4AE', '#78909C', '#607D8B', '#546E7A', '#455A64', '#37474F', '#263238']
}

maps.primary = maps[primaryColor]

export const shade = 7

export const yellow = maps.yellow[shade]
export const red = maps.red[shade]
export const green = maps.green[shade]
export const blue = maps.blue[shade]
export const teal = maps.teal[shade]
export const purple = maps.purple[shade]
export const pink = maps.pink[shade]
export const indigo = maps.indigo[shade]
export const lime = maps.lime[shade]
export const navy = maps.navy[shade]

export const transparent = 'rgba(0, 0, 0, 0)'

export const colors = {yellow, red, green, blue, teal, purple, pink, indigo, lime, navy}

function isRgb (value) {
  return value.search('rgb') !== -1
}

function componentToHex (c) {
  const hex = c.toString(16)
  return hex.length === 1 ? `0${hex}` : hex
}

function rgbToHex (value) {
  if (!isRgb(value)) {
    throw Error('Parameter should be a rgb string')
  }
  const [r, g, b] = value.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/)
  return `#${componentToHex(r)}${componentToHex(g)}${componentToHex(b)}`
}

// Get brightness from hex code
export function getBrightness (value) {
  let hexCode = isRgb(value) ? rgbToHex(value) : value
  // strip off any leading #
  hexCode = hexCode.replace('#', '')

  const r = parseInt(hexCode.substr(0, 2), 16)
  const g = parseInt(hexCode.substr(2, 2), 16)
  const b = parseInt(hexCode.substr(4, 2), 16)

  return ((r * 299) + (g * 587) + (b * 114)) / 1000
}

function hexToRgb (hex) {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)
  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16)
  } : null
}

export function getColor (name, shade = 5, alpha = 1) {
  if (!maps[name]) {
    return null
  }
  const color = maps[name][shade]
  if (alpha === 1) return color
  if (alpha > 1) alpha /= 100
  const {r, g, b} = hexToRgb(color)
  return `rgba(${r}, ${g}, ${b}, ${alpha})`
}

// black shades
export const black = (opacity = 1) => {
  if (opacity > 1) opacity /= 100
  return `rgba(0, 0, 0, ${opacity})`
}

// white shades
export const white = (opacity = 1) => {
  if (opacity > 1) opacity /= 100
  return `rgba(255, 255, 255, ${opacity})`
}

// grey shades
export const grey = (shade = 5, alpha = 1) => {
  return getColor('grey', shade, alpha)
}

// Primary color
export const primary = (shade = 5, alpha = 1) => {
  return getColor(primaryColor, shade, alpha)
}

// Secondary color
export const secondary = (shade = 5, alpha = 1) => {
  return getColor(secondaryColor, shade, alpha)
}

export function lighten (col, amt) {
  let usePound = false

  if (col[0] === '#') {
    col = col.slice(1)
    usePound = true
  }

  const num = parseInt(col, 16)

  let r = (num >> 16) + amt
  if (r > 255) r = 255
  else if (r < 0) r = 0

  let b = ((num >> 8) & 0x00FF) + amt
  if (b > 255) b = 255
  else if (b < 0) b = 0

  let g = (num & 0x0000FF) + amt
  if (g > 255) g = 255
  else if (g < 0) g = 0

  const code = (g | (b << 8) | (r << 16)).toString(16)
  return usePound ? '#'.concat(code) : code
}

export const darken = (col, amt) => lighten(col, amt * -1)

export default {
  maps,
  colors,
  rgbToHex,
  hexToRgb,
  getBrightness,
  white,
  black,
  grey,
  primary,
  secondary,
  lighten,
  darken
}
