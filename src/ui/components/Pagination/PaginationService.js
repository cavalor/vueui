const _data = new WeakMap()
const _items = new WeakMap()
const _page = new WeakMap()
const _perPage = new WeakMap()
const _query = new WeakMap()

export function paginate (data, opts = {}) {
  const page = opts.page || 1
  const perPage = opts.perPage || 10

  return data.slice((page - 1) * perPage, page * perPage)
}

export class PaginateDataset {
  constructor (data = [], opts = {}) {
    _query.set(this, '')
    _page.set(this, opts.page || 1)
    _perPage.set(this, opts.perPage || 10)
    this.data = data
  }

  onChange (cb) {

  }

  updatePagination ({ size, page }) {
    this.perPage = size
    this.page = page
  }

  filter (query) {
    if (!query) {
      query = _query.get(this)
    } else {
      _query.set(this, query)
    }

    if (!this.data.length) {
      return _items.set(this, [])
    }

    _items.set(this, this.data.filter((item) => {
      for (const k in this.data[0]) {
        if (String(item[k]).includes(query)) {
          return true
        }
      }

      return false
    }))
  }

  get data () {
    return _data.get(this)
  }

  set data (data) {
    _data.set(this, Object.assign([], data))
    this.filter()
  }

  get items () {
    return _items.get(this)
  }

  get pageItems () {
    return [...this.items.slice((this.page - 1) * this.perPage, this.page * this.perPage)]
  }

  get page () {
    this.filter()
    return _page.get(this)
  }

  set page (page) {
    if (page === this.page) {
      return
    }
    _page.set(this, page)
  }

  get perPage () {
    return _perPage.get(this)
  }

  set perPage (perPage) {
    if (perPage === this.perPage) {
      return
    }
    _perPage.set(this, perPage)
  }

}

export default PaginateDataset
