import Notify from './Notify'
import Notification from './Notification'
import NotificationsContainer from './NotificationsContainer'

export const notify = new Notify()

export const plugin = {
  install(Vue) {
    Vue.prototype.$notify = notify
    Vue.component('ui-notifications-container', NotificationsContainer)
    Vue.component('ui-notification', Notification)
  }
}

export default notify
