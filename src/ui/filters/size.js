export const size = val => {
  if (val < 2 ** 10) return `${val} B`
  if (val < 2 ** 20) return `${Math.floor(val / 2 ** 10 * 1000) / 1000} kB`
  if (val < 2 ** 30) return `${Math.floor(val / 2 ** 20 * 1000) / 1000} MB`
  else return `${Math.floor(val / 2 ** 30 * 1000) / 1000} TB`
}
export default size
