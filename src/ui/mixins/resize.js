import { debounce } from 'lodash'
import { bus } from 'ui/bus'

const handlers = new WeakMap()

export default {
  mounted () {
    if (typeof this.handleResize !== 'function') {
      throw new Error('Please implement \'handleResize\' method')
    } else {
      handlers.set(this, debounce(e => {
        this.handleResize(e)
      }, 33))
      window.addEventListener('resize', handlers.get(this), false)
      bus.$on('resize', handlers.get(this))
    }
  },

  beforeDestroy () {
    if (handlers.has(this)) {
      window.removeEventListener('resize', handlers.get(this))
      bus.$off('resize', handlers.get(this))
    }
  }
}
