export default {
  // When the bound element is inserted into the DOM...
  inserted: function (el, bindings, {context}) {
    // Focus the element
    context.$bus.$emit('focus', el)
    el.focus()
  }
}
