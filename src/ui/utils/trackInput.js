import {bus} from 'ui/bus'

export default function trackInput (ui) {
  ui.currentInput = 'initial'

  // list of modifier keys commonly used with the mouse and
  // can be safely ignored to prevent false keyboard detection
  const ignoreMap = [
    16, // shift
    17, // control
    18, // alt
    91, // Windows key / left Apple cmd
    93 // Windows menu / right Apple cmd
  ]

  // mapping of events to input types
  const inputMap = {
    keydown: 'keyboard',
    keyup: 'keyup',
    mousedown: 'mouse',
    click: 'mouse',
    touchstart: 'touch'
  }

  const docEl = window.document.documentElement
  for (const eventName in inputMap) {
    docEl.addEventListener(eventName, e => {
      if (ignoreMap.indexOf(e.keyCode) > 0) {
        return
      }

      // ctrl + s || cmd + s
      if (e.keyCode === 83 && (navigator.platform.match('Mac') ? e.metaKey : e.ctrlKey)) {
        e.preventDefault()
        if (e.type === 'keyup') {
          bus.$emit('save')
        }
      }

      if (e.keyCode === ui.keyCodes.escape) {
        bus.$emit('escape')
      }

      ui.currentInput = inputMap[eventName]
    }, false)
  }

  bus.$on('focus', () => {
    ui.currentInput = 'focus'
  })
}
